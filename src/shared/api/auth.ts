import { Types } from '@/entities/user';

import { httpService } from '@/shared/services';

export interface IRegisterRequestDto {
  username: string;
  email: string;
  password: string;
}

export interface IRegisterResponseDto {
  jwt: string;
  user: Types.IUser;
}

export interface ILoginRequestDto {
  identifier: string;
  password: string;
}

export interface ILoginResponseDto extends IRegisterResponseDto {}

export const register = (dto: IRegisterRequestDto) => {
  return httpService.post<IRegisterResponseDto>('/auth/local/register', dto);
};

export const login = (dto: ILoginRequestDto) => {
  return httpService.post<ILoginResponseDto>('/auth/local', dto);
};
