import {httpService} from "@/shared/services";

export const fetchMe = () => httpService.get('/users/me')
