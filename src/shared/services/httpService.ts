import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

const get = <T>(url: string) => axiosInstance.get<T>(url);

const post = <T>(url: string, data: any) => axiosInstance.post<T>(url, data);

export default {
  get,
  post,
};
