import { defineStore } from 'pinia';
import type { AxiosError } from 'axios';
import { reactive, ref } from 'vue';
import { useRouter } from 'vue-router';
import { email, helpers, required } from '@vuelidate/validators';

import { authApi } from '@/shared/api';
import useVuelidate from '@vuelidate/core';

export const useRegisterStore = defineStore('register', () => {
  const router = useRouter();

  const form = reactive({
    username: '',
    email: '',
    password: '',
  });
  const error = ref<null | string>(null);
  const isSubmitting = ref(false);

  const register = async () => {
    const isFormValid = await v$.value.$validate();

    if (!isFormValid) return;

    isSubmitting.value = true;

    try {
      await authApi.register(form);
      router.push({ name: 'login' });
    } catch (e: AxiosError) {
      error.value = e.message;
    }

    isSubmitting.value = false;
  };

  const rules = {
    username: { required: helpers.withMessage('Обязательное поле', required) },
    email: {
      required: helpers.withMessage('Обязательное поле', required),
      email: helpers.withMessage('Должно быть валидным email адресом', email),
    },
    password: { required: helpers.withMessage('Обязательное поле', required) },
  };

  const v$ = useVuelidate(rules, form);

  return { form, error, isSubmitting, register, v$ };
});
