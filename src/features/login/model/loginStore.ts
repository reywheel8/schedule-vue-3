import { defineStore } from 'pinia';
import { computed, reactive, ref } from 'vue';
import { useRouter } from 'vue-router';
import { helpers, required } from '@vuelidate/validators';
import useVuelidate from '@vuelidate/core';

import { authApi } from '@/shared/api';
import { Types } from '@/entities/user';

export const useLoginStore = defineStore('login', () => {
  const router = useRouter();

  const user = ref<null | Types.IUser>(null);
  const form = reactive({
    identifier: '',
    password: '',
  });
  const error = ref<null | string>(null);
  const isSubmitting = ref(false);

  const isAuth = computed(() => user.value === null);
  const isAnonym = computed(() => !isAuth.value);

  const login = async () => {
    const isFormCorrect = await v$.value.$validate();

    if (!isFormCorrect) return;

    isSubmitting.value = true;

    try {
      const response = await authApi.login(form);
      user.value = response.data.user;
      router.push({ name: 'home' });
    } catch (e) {
      error.value = e.response.message;
    }

    isSubmitting.value = false;
  };

  const rules = {
    identifier: {
      required: helpers.withMessage('Обязательное поле', required),
    },
    password: {
      required: helpers.withMessage('Обязательное поле', required),
    },
  };

  const v$ = useVuelidate(rules, form);

  return { isAuth, isAnonym, form, isSubmitting, login, v$ };
});
